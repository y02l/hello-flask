# Hello と返すだけの flaskサーバです

サービス間の連携・CI環境構築のために作られた、Helloと返すflaskサーバです

## クイックスタート
```
git clone git@gitlab.com:y02l/hello-flask.git; \
cd hello-flask; \
docker-compose up -d; \
curl http://$(docker port hello-flask_hello-flask_1 80)/
```

## ローカルでのテスト
```
docker-compose exec hello-flask pytest /app/test
```

## APIリファレンス
### ハロー
#### HTTPリクエスト
```
GET http://hello-flask/
```

#### レスポンス
text/plane
```
"Hello"
```
