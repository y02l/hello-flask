from run import app


def test_get_root():
    app.config['TESTING'] = True
    client = app.test_client()
    resp = client.get('/', content_type='text/plain')

    assert resp.status_code == 200
    assert resp.get_data().decode('UTF-8') == 'Hello'
